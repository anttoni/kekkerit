<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script defer data-domain="kekkerit.fi" src="https://plausible.io/js/script.js"></script>
        <title>Kekkerit.fi - Muumimaraton VII - 14.-17.9.2023</title>
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="antialiased">
        <div class="p-6">
            <section class="border-b py-4">
                <h1 class="text-3xl mb-4">
                Muumimaraton VII - 14.-17.9.2023 @ Hämeenlinna
                </h1>
            </section>
            <div class="grid grid-cols-1 md:grid-cols-2">
                @livewire('episode-progress')
                @livewire('episode-tells')
            </div>
            <div class="p-6">
            @livewire('random-quote')
            @livewire('quote-search')
            <h3 class="text-3xl">Siirin maratoonin aikana virkkaama vatti</h3>
            <img class="w-64" src="{{asset('storage/vatti.jpg')}}">
            </div>
        </div>
    </body>
</html>
