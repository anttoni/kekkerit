<div>
    <section class="border-b py-6">
        <h2 class="text-2xl">Edistyminen</h2>
        <p class="text-xl">{{$maraton->episode}} / 104</p>
    </section>
    <section class="border-b py-4">
        @if($maraton->on_break)
            <h2 class="text-2xl mt-4">Tauko päällä</h2>
            <p class="text-xl">{{$maraton->break_reason}}</p>
        @else
            <h2 class="text-2xl">Nyt pyörii</h2>
            <p class="text-xl">{{$maraton->episode}}. <span class="italic">{{$episode}} </span></p>
        @endif
    </section>
    <section class="border-b py-4">
        <h2 class="text-2xl mt-4">Jaksojärjestys</h2>
        <ul>
            @foreach($episodeList as $episodeItem)
                <li>
                    @if($episode === $episodeItem)
                        <strong> {{$maraton->on_break ? "⏸" : "▶️" }}
                    @endif
                    
                    {{$episodeItem}}
                    @if($episode === $episodeItem)
                        </strong> 
                    @endif
                </li>
            @endforeach
        </ul>
        
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" wire:click="incrementEpisode">⬇</button>
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" wire:click="decrementEpisode">⬆</button>
    </section>
</div>
