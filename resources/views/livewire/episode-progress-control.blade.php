<div>
    <section class="border-b py-4">
        <h2 class="text-3xl mt-4">Jaksohallinta</h2>
        @if($maraton->on_break)
            <p class="text-2xl py-4"> Tauko päällä</p>
        @else 
            <p class="text-2xl py-4"> Nyt pyörii:  {{$maraton->episode}}. {{$episode}}</p>
        @endif
        <ul>
            @foreach($episodeList as $episodeItem)
                <li>
                    @if($episode === $episodeItem)
                        <strong> {{$maraton->on_break ? "⏸" : "▶️" }}
                    @endif
                    
                    {{$episodeItem}}
                    @if($episode === $episodeItem)
                        </strong> 
                    @endif
                </li>
            @endforeach
        </ul>
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" wire:click="previousEpisode">⏮</button>
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" wire:click="toggleBreak">{{$maraton->on_break ? "▶️" : "⏸" }}</button>
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" wire:click="nextEpisode">⏭</button>
        @if($maraton->on_break)
        <input type="text" wire:model.defer="breakReason" />
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" wire:click="saveBreakReason">Tallenna tauon syy</button>
        @endif
    </section>
</div>
