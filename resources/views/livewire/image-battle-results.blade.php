<div class="p-4">
    <div class="max-w-lg">
        <h1 class="text-3xl md:text-4xl">Jaksotaiston tulokset</h1>
        <div class="rounded bg-white shadow px-6 py-4 my-4">
            <ul>
                <h2 class="text-2xl">Top 3</h1>
                @for( $i = 0; $i < 3; $i++)
                    <li>{{$i+1}}. {{$episodes[$results[$i]]}} #{{$results[$i]}}</li>
                @endfor
            </ul>
        </div>
        <div class="rounded bg-white shadow px-6 py-4 mb-4">
            <h2 class="text-2xl">Bottom 3</h2>
            <ul class="my-4">
                @for( $i = count($results) - 3; $i < count($results) ; $i++)
                    <li>{{$i+1}}. {{$episodes[$results[$i]]}} #{{$results[$i]}}</li>
                @endfor
            </ul>
        </div>

        <div x-data="{ expanded: 0 }" role="region" class="rounded bg-white shadow">
            <h2 class="text-2xl">
                <button
                    x-on:click="expanded = !expanded"
                    :aria-expanded="expanded"
                    class="flex w-full items-center justify-between px-6 py-4 text-2xl"
                >
                Koko lista
                    <span x-show="expanded" aria-hidden="true" class="ml-4">&minus;</span>
                    <span x-show="!expanded" aria-hidden="true" class="ml-4">&plus;</span>
                </button>
            </h2>
        
            <div x-show="expanded" x-collapse>
                <div class="px-6 pb-4">
                    <ul class="my-4">

                        @foreach( $results as $index => $result)
                            <li class="grid grid-cols-6">
                                <div class="col-span-4 text-sm">{{$index + 1}}. {{$episodes[$result]}} #{{$result}}</div>
                                <div class="text-xs">
                                    JÄ:
                                    <div class=" @if (abs(104 - $index - $relativeAvgs[$result]) < 5) bg-green-200
                                        @elseif( abs(104 - $index - $relativeAvgs[$result]) < 15)
                                            bg-yellow-200
                                        @elseif( abs(104 - $index - $relativeAvgs[$result]) < 30)
                                            bg-red-200
                                        @else 
                                            bg-red-500 text-white
                                        @endif
                                            inline-block px-2 py-1 rounded border
                                        ">{{  104 - $index - $relativeAvgs[$result]}}
                                    </div>
                                </div>
                                <div class="text-xs">
                                    KA:
                                    <div class=" @if (abs(104 - $index - $averages[$result]) < 5) bg-green-200
                                        @elseif( abs(104 - $index - $averages[$result]) < 15)
                                            bg-yellow-200
                                        @elseif( abs(104 - $index - $averages[$result]) < 30)
                                            bg-red-200
                                        @else 
                                            bg-red-500 text-white
                                        @endif
                                            inline-block px-2 py-1 rounded border
                                        ">{{  104 - $index - $averages[$result]}}
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
