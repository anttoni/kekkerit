<div class="p-4 border">

    <h2 class="text-2xl">{{$currentEpisodeId}}. {{$episode}}</h2>

    @if( $quotes->count() > 0 )
        <h3 class="text-xl mt-4">Lainauksia jaksosta</h3>
        <div class="py-2">
            <blockquote class="text-sm">"{{$quote}}"</p>
            <p class="text-xs">- {{$character}}</p>
        </div>
        @if( count($quotes) > 1 )
            <div class="mt-4">
                <button wire:click="nextQuote" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Arvo uusi 🔁</button>
            </div>
        @endif
    @endif

    @if( $thoughts->count() > 0 )
    <h3 class="text-xl mt-4 py-4 border-t">Ajatuksia jaksosta viimevuosilta</h3>
    <div class="py-4">
        <blockquote class="text-sm">"{{$tell}}"</p>
        <p class="text-xs">- {{$teller}}</p>
    </div>
    @endif
    
    @if( count($thoughts) > 1 )
        <div class="mt-4 mb-5">
            <button wire:click="nextTell" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Arvo uusi 🔁</button>
        </div>
    @endif

    <h3 class="text-3xl border-t py-3">2019 arvostelun keskiarvot</h3>
    <div class="py-2">
        <p class="text-5xl mt-4"><span class="text-2xl">Keskiarvo</span> {{$averageGrade}}<span class="text-3xl">/10</p>
        <p class="text-5xl mt-4"><span class="text-2xl">Mediaani</span> {{$medianGrade}}<span class="text-3xl">/10</p>
    </div>

    <div class="mt-4">
        Selaa jaksoja:<br>
        <button class="p-2 border bg-blue-500 hover:bg-blue-700 rounded text-white text-sm" wire:click="prevEpisode">⏪ Edellinen jakso </button>
        <button class="p-2 border bg-blue-500 hover:bg-blue-700 rounded text-white text-sm" wire:click="nextEpisode">Seuraava jakso ⏩</button>
    </div>

</div>
