<div>
    <h2 class="py-4 text-3xl">Lainaushaku</h2>
    <input 
    class="border border-gray-400 p-2"
    type="text" wire:model.live.defer="searchTerm"/>
    @if( empty($results) && empty($searchTerm))
        <p>Kirjoita hakusana</p>
    @elseif( empty($results))
        <p>Ei löydy.</p>
    @else
    <h3 class="text-2xl py-2">Tulokset</h3>
    <ul>
        @foreach( $results as $result ) 
            <li>"{{$result['item']->quote}}" - {{$result['item']->character}} </i></li>
        @endforeach
    </ul>
    @endif
</div>
