<div>
    <h2 class="py-4 text-3xl">Lainauslisäin</h2>
    <form wire:submit.prevent class="grid grid-cols-2 gap-2">
        <div class="flex flex-col col-span-1"><span>Jakso</span>
            <select wire:model="episode">
                @for( $i = 1; $i <= 104; $i++ )
                    <option value="{{$i}}">{{$i}}. {{$episodes[$i]}}</option>
                @endfor
            </select>
            @error('episode') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="flex flex-col col-span-1"><span>Hahmo</span><input wire:model.live="character" type="text">
            @error('character') <span class="error">{{ $message }}</span> @enderror</div>
        
        <div class="col-span-2 flex flex-col">
            <span>Lainaus</span>
            <input 
            class="border border-gray-400 p-2 w-full"
            type="text" wire:model.live.debounce="quotation"/>
            <div class="my-2">
                <button wire:click="saveQuote"
                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Tallenna
                </button>
            </div>
            @error('quotation') <span class="error">{{ $message }}</span> @enderror
            @if( empty($results))
                <p>Ei vastaavia lainauksia.</p>
            @else
            <h3 class="text-2xl py-2">Mahdollisia osumia</h3>
            <ul>
                @foreach( $results as $result ) 
                    <li wire:key="result-{{$result['item']->id}}">"{{$result['item']->quote}}" - {{$result['item']->character}} </i></li>
                @endforeach
            </ul>
            @endif
        </div>
    </form>
    <div class="col-span-2">
        <h3 class="text-2xl py-2">Uusimmat lainaukset</h3>
        <ul>
            @foreach( $latestQuotes as $quote ) 
                <li wire:key="quote-{{$quote->id}}">"{{$quote->quote}}" - {{$quote->character}} </i></li>
            @endforeach
        </ul>
    </div>
</div>
