<div class="grid grid-cols-1 md:grid-cols-2 h-3/4 h-screen overflow-hidden relative"
    x-data="{
        content: 'Ota talteen linkki niin pääset peliin myöhemmin:',
        type: 'info',
    }"
    class="max-w-sm w-full"
    x-init="$dispatch('notify', { content, type })"
>
    <button class="flex flex-col items-center justify-center border text-2xl md:text-4xl p-4 md:p-12 h-full animate__animated h-full animate__backInLeft bg-blue-200 hover:bg-blue-300" wire:click="win">
        <h2 class="max-w-xl">{{$battleItem['id']}}. {{$battleItem['name']}}</h2>
        <p class="max-w-xl text-xs py-6">{{$battleItem['description']}}</p>
    </button>

    <button class="flex flex-col items-center justify-center border text-2xl md:text-4xl p-4 md:p-12 h-full animate__animated h-full {{$vsItemAnimation}} bg-green-200 hover:bg-green-300" wire:click="lose">
        <h2 class="max-w-xl">{{$vsItem['id']}}. {{$vsItem['name']}}</h2>
        <p class="max-w-xl text-xs py-6">{{$vsItem['description']}}</p>
    </button>
    
    {{-- Add absolute bar on bottom of the page --}}
    <div class="absolute bottom-0 w-[calc(100%-6rem)] p-1 bg-purple-200 overflow-x-auto overflow-y-hidden h-10 whitespace-nowrap">
        @foreach( $results as $result)
            {{$result}}.
        @endforeach
    </div>

    <button class="absolute bottom-0 right-0 p-1 bg-blue-500 hover:bg-blue-700 text-white h-10 whitespace-nowrap w-24"wire:click="save">
       Tallenna
    </button>

    <div
        x-data="{
            notifications: [],
            add(e) {
                this.notifications.push({
                    id: e.timeStamp,
                    type: e.detail.type,
                    content: e.detail.content,
                })
            },
            remove(notification) {
                this.notifications = this.notifications.filter(i => i.id !== notification.id)
            },
        }"
        @notify.window="add($event)"
        class="fixed top-0 right-0 flex w-full max-w-lg flex-col space-y-4 pr-4 pb-4 sm:justify-start"
        role="status"
        aria-live="polite"
    >
            <!-- Notification -->
            <template x-for="notification in notifications" :key="notification.id">
                <div
                    x-data="{
                        show: false,
                        init() {
                            this.$nextTick(() => this.show = true)
        
                        },
                        transitionOut() {
                            this.show = false
        
                            setTimeout(() => this.remove(this.notification), 500)
                        },
                    }"
                    x-show="show"
                    x-transition.duration.500ms
                    class="pointer-events-auto relative w-full max-w-lg rounded-md border border-gray-200 bg-white p-4 shadow-lg"
                >
                    <div class="flex items-start">
                        <!-- Icons -->
                        <div x-show="notification.type === 'info'" class="flex-shrink-0">
                            <span aria-hidden="true" class="inline-flex h-6 w-6 items-center justify-center rounded-full border border-gray-400 text-sm font-bold text-gray-400">!</span>
                            <span class="sr-only">Information:</span>
                        </div>
        
                        <!-- Text -->
                        <div class="ml-3 w-0 flex-1 pt-0.5">
                            <p x-text="notification.content" class="text-sm font-medium leading-5 text-gray-900"></p>
                            <input class="text-xs p-1 w-64" value="{{$url}}" disabled> <button class="border text-xs p-1 bg-green-200 w-16" onclick="navigator.clipboard.writeText('{{$url}}'); this.innerText='Ju!'";>Kopioi</button>
                        </div>
        
                        <!-- Remove button -->
                        <div class="ml-4 flex flex-shrink-0">
                            <button @click="transitionOut()" type="button" class="inline-flex text-gray-400">
                                <svg aria-hidden class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </template>
        </div>
    {{-- Add VS text on the middle of the parent element --}}
    <div class="{{$vsTextClass}} absolute top-1/2 left-1/2 -m-8 text-5xl font-bold">
        VS
    </div>
</div>
