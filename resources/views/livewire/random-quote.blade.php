<div>
    <h2 class="text-3xl py-4">Lainauksia jaksoista</h2>
    <blockquote
    class="text-2xl italic border-l-4 border-blue-500 pl-4 my-4"
    > "{{ $quote}}"</blockquote>
    <span class="text-xl block">- {{ $character}} jaksossa <i>{{ $episode}}</i></span>
    <br>
    <button
    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
    wire:click="getRandomQuote">Arvo uusi 🔁</button>
</div>
