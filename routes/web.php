<?php

use App\Http\Controllers\ProfileController;
use App\Livewire\ImageBattle;
use App\Livewire\ImageBattleResults;
use App\Models\Battle;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/jaksotaisto', function () {
    $faker = Faker\Factory::create();
    $keys = config('muumi.keys');

    $battle = Battle::create([
        'battle_key' => $keys[array_rand($keys)].'-'.$faker->randomNumber(2),
    ]);

    return redirect()->route('jaksotaisto', $battle->battle_key);

    return view('jaksotaisto');
})->name('jaksotaisto-new');

Route::get('/jaksotaisto/{battle:battle_key}', ImageBattle::class)->name('jaksotaisto');

Route::get('/jaksotaisto/tulokset/{battle:battle_key}', ImageBattleResults::class)->name('jaksotaisto-tulokset');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
