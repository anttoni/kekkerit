<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('maraton', function (Blueprint $table) {
            // Add column after on_break
            $table->string('break_reason')->after('on_break')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('maraton', function (Blueprint $table) {
            // Drop column
            $table->dropColumn('break_reason');
        });
    }
};
