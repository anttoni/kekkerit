<?php

namespace App\Livewire;

use App\Models\Quotation;
use Livewire\Component;

class RandomQuote extends Component
{
    public $quote;

    public $character;

    public $episode;

    public $episodes;

    public function render()
    {
        return view('livewire.random-quote');
    }

    public function mount()
    {
        $this->episodes = config('muumi.episodes');
        $this->getRandomQuote();
    }

    public function getRandomQuote()
    {
        $quote = Quotation::inRandomOrder()->first();
        $this->quote = $quote->quote;
        $this->character = $quote->character;
        $this->episode = $this->episodes[$quote->episode];
    }
}
