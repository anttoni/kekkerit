<?php

namespace App\Livewire;

use App\Models\Quotation;
use Fuse\Fuse;
use Livewire\Component;

class QuoteSearch extends Component
{
    public $quotes;

    public array $results;

    public $searchTerm;

    public function render()
    {
        return view('livewire.quote-search');
    }

    public function mount()
    {
        $this->quotes = Quotation::all();
        $this->results = [];
        $this->searchTerm = '';
    }

    public function updatedSearchTerm()
    {
        $fuse = new Fuse($this->quotes->all(), [
            'keys' => ['quote'],
            'threshold' => 0.3,
        ]);

        $this->results = $fuse->search($this->searchTerm);
    }
}
