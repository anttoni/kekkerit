<?php

namespace App\Livewire;

use App\Models\Maraton;
use App\Models\Quotation;
use Fuse\Fuse;
use Illuminate\Support\Collection;
use Livewire\Attributes\Rule;
use Livewire\Component;

class QuoteCreator extends Component
{
    public $quotes;

    public array $results;

    #[Rule('required|min:5')]
    public string $quotation = '';

    #[Rule('required|integer')]
    public int $episode = 1;

    public array $episodes;

    public Collection $latestQuotes;

    #[Rule('required')]
    public string $character = '';

    public function render()
    {
        return view('livewire.quote-creator');
    }

    public function mount()
    {
        $this->quotes = Quotation::all();
        $this->results = [];
        $this->quotation = '';
        $this->episode = Maraton::find(1)->episode;
        $this->episodes = config('muumi.episodes');
        // Get 5 quotes from the database descending by id
        $this->latestQuotes = $this->quotes->sortByDesc('id')->take(5);
    }

    public function updatedQuotation()
    {
        $fuse = new Fuse($this->quotes->all(), [
            'keys' => ['quote'],
            'threshold' => 0.3,
        ]);

        $this->results = $fuse->search($this->quotation);
    }

    public function saveQuote()
    {

        $this->validate();

        $quote = Quotation::create([
            'quote' => $this->quotation,
            'episode' => $this->episode,
            'character' => $this->character,
        ]);

        $this->quotes->push($quote);
        $this->latestQuotes = $this->quotes->sortByDesc('id')->take(5);
        $this->reset('quotation', 'character');
    }
}
