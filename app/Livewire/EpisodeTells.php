<?php

namespace App\Livewire;

use App\Models\Maraton;
use App\Models\Quotation;
use App\Models\Tell;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class EpisodeTells extends Component
{
    public $episode = '';

    public ?Collection $quotes;

    public Maraton $maraton;

    public string $quote = '';

    public string $character = '';

    public Collection $thoughts;

    public ?Collection $tells;

    public string $tell = '';

    public string $teller = '';

    public float $averageGrade = 0;

    public float $medianGrade = 0;

    public int $currentEpisodeId = 0;

    public function render()
    {
        return view('livewire.episode-tells');
    }

    public function mount()
    {
        $this->maraton = Maraton::find(1);
        $this->episode = $this->maraton->getEpisode();
        $this->currentEpisodeId = $this->maraton->episode;

        $this->getQuotes();
        $this->nextQuote();

        $this->getTells();
        $this->getThoughts();
        $this->nextTell();

        $this->getAverageGrade();
        $this->getMedianGrade();
    }

    public function getQuotes()
    {
        $quotes = Quotation::where('episode', $this->currentEpisodeId)->get();
        $this->quotes = $quotes->filter(function ($quote) {
            return $quote->quote !== $this->quote;
        });
    }

    public function nextQuote()
    {

        if ($this->quotes->count() < 1) {
            return;
        }

        $randomQuote = $this->quotes->random();

        $this->quote = $randomQuote->quote;
        $this->character = $randomQuote->character;
    }

    public function getTells()
    {
        $this->tells = Tell::query()
            ->where('episode', $this->currentEpisodeId)
            ->get();
    }

    public function getThoughts()
    {
        $this->thoughts = $this->tells->filter(function ($tell) {
            return $tell->tell != '' && $tell->tell !== $this->tell;
        });
    }

    public function nextTell()
    {

        if ($this->thoughts->count() < 1) {
            return;
        }

        $randomTell = $this->thoughts->random();

        $this->tell = $randomTell->tell;
        $this->teller = $randomTell->teller;
    }

    public function getAverageGrade()
    {

        $grade = $this->tells->pluck('grade')->avg();
        // Round to two decimals
        $this->averageGrade = round($grade, 2);
    }

    public function getMedianGrade()
    {

        $grades = $this->tells->pluck('grade')->sort()->values();
        $count = $grades->count();
        $middle = floor(($count - 1) / 2);

        if ($count % 2) {
            $median = $grades[$middle];
        } else {
            $low = $grades[$middle];
            $high = $grades[$middle + 1];
            $median = (($low + $high) / 2);
        }

        $this->medianGrade = $median;
    }

    public function nextEpisode()
    {
        if ($this->currentEpisodeId >= 104) {
            return;
        }

        $this->getQuotes();
        $this->nextQuote();

        $this->getTells();
        $this->getThoughts();
        $this->nextTell();

        $this->getAverageGrade();
        $this->getMedianGrade();

        $this->currentEpisodeId++;

        $episodes = config('muumi.episodes');
        $this->episode = $episodes[$this->currentEpisodeId];
    }

    public function prevEpisode()
    {
        if ($this->currentEpisodeId <= 1) {
            return;
        }

        $this->getQuotes();
        $this->nextQuote();

        $this->getTells();
        $this->getThoughts();
        $this->nextTell();

        $this->getAverageGrade();
        $this->getMedianGrade();

        $this->currentEpisodeId--;

        $episodes = config('muumi.episodes');
        $this->episode = $episodes[$this->currentEpisodeId];

    }
}
