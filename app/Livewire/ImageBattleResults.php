<?php

namespace App\Livewire;

use App\Models\Battle;
use Livewire\Component;

class ImageBattleResults extends Component
{
    public Battle $battle;

    public array $results = [];

    public array $episodes;

    public array $averages = [];

    public array $relativeAvgs = [];

    public function render()
    {
        return view('livewire.image-battle-results');
    }

    public function mount(Battle $battle)
    {
        $this->battle = $battle;
        $this->episodes = config('muumi.episodes');
        $this->results = $this->battle->results;
        $this->averages = config('muumi.averages');
        $this->relativeAvgs = config('muumi.relativeAvg');

    }
}
