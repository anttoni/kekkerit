<?php

namespace App\Livewire;

use App\Models\Battle;
use App\Models\Item;
use Illuminate\Support\Collection;
use Livewire\Component;

class ImageBattle extends Component
{
    public array $battleSequence = [];

    public array $results = [];

    public array $itemsToBattle = [];

    public array $vsItem;

    public int $vsItemNumber;

    public array $battleItem;

    public int $battleItemNumber;

    public array $vsItemAnimations = [
        'animate__backInDown',
        'animate__backInRight',
        'animate__backInUp',
    ];

    public string $battleItemAnimation = 'animate__backInLeft';

    public string $vsItemAnimation = '';

    public Collection $items;

    public Battle $battle;

    public string $vsTextClass = '';

    public string $url = '';

    public function render()
    {
        return view('livewire.image-battle');
    }

    public function mount(Battle $battle)
    {

        $this->items = Item::all();

        $this->battle = $battle;
        if (! $this->battle->sequence) {
            $this->battleSequence = $this->items->pluck('id')->shuffle()->all();
        } else {
            $this->battleSequence = $this->battle->sequence;
        }

        if (! $this->battle->results) {
            $this->results = array_splice($this->battleSequence, 0, 1);
            $this->itemsToBattle = $this->results;
            $this->vsItemNumber = $this->itemsToBattle[0];
        } else {
            $this->results = $this->battle->results;
            $this->itemsToBattle = $this->results;
            $middleItem = (int) floor(count($this->itemsToBattle) / 2);
            $this->vsItemNumber = $this->itemsToBattle[$middleItem];
        }

        if (empty($this->battleSequence) || count($this->results) > 103) {
            $this->redirect(route('jaksotaisto-tulokset', $this->battle->battle_key));
        }

        $this->battleItemNumber = array_splice($this->battleSequence, 0, 1)[0];
        $this->battleItem = $this->items[$this->battleItemNumber - 1]->toArray();

        $this->vsItem = $this->items[$this->vsItemNumber - 1]->toArray();
        $this->vsItemAnimation = $this->vsItemAnimations[array_rand($this->vsItemAnimations)];
        $this->vsTextClass = 'animate__animated animate__flipInY';
        $this->url = route('jaksotaisto', $battle->battle_key);

    }

    public function nextBattle()
    {
        if (empty($this->battleSequence)) {
            $this->battle->results = $this->results;
            $this->battle->save();

            $this->redirect(route('jaksotaisto-tulokset', $this->battle->battle_key));

        } else {

            $this->battleItemNumber = array_splice($this->battleSequence, 0, 1)[0];
            $this->battleItem = $this->items[$this->battleItemNumber - 1]->toArray();
            $this->itemsToBattle = $this->results;
            $this->togglebattleItemAnimation();
        }
    }

    public function win()
    {
        $middleItem = (int) floor(count($this->itemsToBattle) / 2);
        $this->nextItem(0, $middleItem);
    }

    public function lose()
    {
        $middleItem = (int) floor(count($this->itemsToBattle) / 2);
        $this->nextItem($middleItem + 1, null, 1);
    }

    public function nextItem($start, $end, $addition = 0)
    {
        $this->itemsToBattle = array_slice($this->itemsToBattle, $start, $end);

        if (empty($this->itemsToBattle)) {
            $insertPosition = array_search($this->vsItemNumber, $this->results) + $addition;
            // Insert the battleitem to $inserposition of $this->results
            array_splice($this->results, $insertPosition, 0, [$this->battleItemNumber]);
            $this->nextBattle();
        }

        if (! empty($this->battleSequence)) {
            $middleItem = (int) floor(count($this->itemsToBattle) / 2);
            $this->vsItemNumber = $this->itemsToBattle[$middleItem];
            $this->vsItem = $this->items[$this->vsItemNumber - 1]->toArray();
            $this->toggleVsItemAnimation();
        }

    }

    public function togglebattleItemAnimation()
    {
        $this->battleItemAnimation = '';
        $this->battleItemAnimation = 'animate__backInLeft';
    }

    public function toggleVsItemAnimation()
    {
        $this->vsItemAnimation = '';
        $this->vsItemAnimation = $this->vsItemAnimations[array_rand($this->vsItemAnimations)];
    }

    public function save()
    {
        $this->battle->sequence = array_merge([$this->battleItemNumber], $this->battleSequence);
        $this->battle->results = $this->results;
        $this->battle->save();
    }
}
