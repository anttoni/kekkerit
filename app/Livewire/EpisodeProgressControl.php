<?php

namespace App\Livewire;

use App\Models\Maraton;
use Livewire\Component;

class EpisodeProgressControl extends Component
{
    public string $episode = '';

    public array $episodes = [];

    public array $episodeList = [];

    public ?Maraton $maraton = null;

    public string $breakReason = '';

    public function render()
    {
        return view('livewire.episode-progress-control');
    }

    public function mount()
    {
        $this->maraton = Maraton::find(1);

        $this->episode = $this->maraton->getEpisode();
        $this->episodes = config('muumi.episodes');
        $this->episodeList = $this->getEpisodeList();
        $this->breakReason = $this->maraton->break_reason;

    }

    public function getEpisodeList()
    {
        // Get three episodes before and after the current episode
        $episodeList = [];
        $currentEpisode = $this->maraton->episode;

        for ($i = $currentEpisode - 3; $i <= $currentEpisode + 3; $i++) {
            if ($i < 1 || $i > count($this->episodes)) {
                $episodeList[] = '-';

                continue;
            }
            $episodeList[] = $this->episodes[$i];
        }

        return $episodeList;

    }

    public function nextEpisode()
    {
        $this->maraton->episode++;
        $this->maraton->save();

        $this->episodeList = $this->getEpisodeList();
    }

    public function previousEpisode()
    {
        $this->maraton->episode--;
        $this->maraton->save();
        $this->episodeList = $this->getEpisodeList();

    }

    public function saveBreakReason()
    {
        $this->maraton->break_reason = $this->breakReason;
        session()->flash('message', 'Tauko !');
        $this->maraton->save();
    }

    public function toggleBreak()
    {
        $this->maraton->on_break = ! $this->maraton->on_break;
        $this->maraton->save();
    }
}
