<?php

namespace App\Livewire;

use App\Models\Maraton;
use Livewire\Component;

class EpisodeProgress extends Component
{
    public $episode;

    public $episodes;

    public $episodeList;

    public $episodeSelector;

    public $maraton;

    public function render()
    {
        return view('livewire.episode-progress');
    }

    public function mount()
    {
        $this->maraton = Maraton::firstOrCreate(
            ['id' => 1],
            ['episode' => 1]
        );

        $this->episode = $this->maraton->getEpisode();
        $this->episodes = config('muumi.episodes');
        $this->episodeSelector = $this->maraton->episode;
        $this->episodeList = $this->getEpisodeList();

    }

    public function getEpisodeList()
    {
        // Get three episodes before and after the current episode
        $episodeList = [];

        for ($i = $this->episodeSelector - 3; $i <= $this->episodeSelector + 3; $i++) {
            if ($i < 1 || $i > count($this->episodes)) {
                $episodeList[] = '-';

                continue;
            }
            $episodeList[] = $this->episodes[$i];
        }

        return $episodeList;

    }

    public function incrementEpisode()
    {
        $this->episodeSelector++;
        $this->episodeList = $this->getEpisodeList();
    }

    public function decrementEpisode()
    {
        $this->episodeSelector--;
        $this->episodeList = $this->getEpisodeList();

    }
}
