<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    use HasFactory;

    protected $fillable = [
        'battle_key',
    ];

    protected function results(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => json_decode($value),
            set: fn (array $value) => json_encode($value),
        );
    }

    protected function sequence(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => json_decode($value),
            set: fn (array $value) => json_encode($value),
        );
    }
}
