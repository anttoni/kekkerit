<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tell extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'teller',
        'episode',
        'grade',
        'tell',
    ];
}
