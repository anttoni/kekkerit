<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'episode',
        'description',
        'image',
    ];

    public static function createItemsFromEpisodes()
    {
        $episodes = config('muumi.episodes');
        $descriptions = config('muumi.descriptions');
        foreach ($episodes as $key => $episode) {
            // Create an item for each episode
            Item::create([
                'name' => $episode,
                'description' => $descriptions[$key],
                'episode' => $key,
                'image' => '#',
            ]);
        }
    }
}
