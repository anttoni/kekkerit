<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maraton extends Model
{
    use HasFactory;

    protected $table = 'maraton';

    protected $attributes = [
        'on_break' => false,
        'break_reason' => '',
    ];

    protected $casts = [
        'on_break' => 'boolean',
    ];

    protected $fillable = [
        'id',
        'episode',
        'on_break',
        'break_reason',
    ];

    public $timestamps = false;

    public function getEpisode()
    {
        $episodes = config('muumi.episodes');

        return $episodes[$this->episode];
    }
}
