<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;

    public $table = 'quotations';

    public $timestamps = false;

    protected $fillable = [
        'quote',
        'episode',
        'character',
    ];
}
